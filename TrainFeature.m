radius=0.6;

line_pts=textread('train\line.xyz');
plane_pts=textread('train\plane.xyz');
volume_pts=textread('train\tree2.xyz');

line_pts=line_pts(:,1:3);
line_pts=intersect(line_pts,V_pts,'rows');

nearest=rangesearch(A,line_pts,radius);

var=zeros(length(line_pts),3);
coeffs=zeros(length(line_pts),3);
for i=1:length(line_pts)
    A_t=A(nearest{i,1},:);
    [coeff,~,variance]=pca(A_t);
    if length(variance)<3
        continue;
    end
    var(i,:)=variance';
    coeffs(i,:)=coeff(:,1);
end

plane_pts=plane_pts(:,1:3);
plane_pts=intersect(plane_pts,V_pts,'rows');

nearest=rangesearch(A,plane_pts,radius);

var2=zeros(length(plane_pts),3);

for i=1:length(plane_pts)
    A_t=A(nearest{i,1},:);
    [~,~,variance]=pca(A_t);
    if length(variance)<3
        continue;
    end
    var2(i,:)=variance';
end


volume_pts=volume_pts(:,1:3);
volume_pts=intersect(volume_pts,V_pts,'rows');

nearest=rangesearch(A,volume_pts,radius);

var3=zeros(length(volume_pts),3);

for i=1:length(volume_pts)
    A_t=A(nearest{i,1},:);
    [coeff,~,variance]=pca(A_t);
    if length(variance)<3
        continue;
    end
    var3(i,:)=variance';
end

varn=sqrt(sum(var.^2,2));
varn(varn==0)=1;
nVarN=bsxfun(@rdivide,var,varn);

varn2=sqrt(sum(var2.^2,2));
varn2(varn2==0)=1;
nVarN2=bsxfun(@rdivide,var2,varn2);

varn3=sqrt(sum(var3.^2,2));
varn3(varn3==0)=1;
nVarN3=bsxfun(@rdivide,var3,varn3);

var=nVarN;
var2=nVarN2;
var3=nVarN3;

var(:,4)=1;
var2(:,4)=2;
var3(:,4)=2;
var_all=[var;var2;var3];
%t=mnrfit(var_all(:,1:3),var_all(:,4));
brobs=robustfit(var_all(:,1:3),var_all(:,4),'logistic');

var2(:,4)=1;
var3(:,4)=2;
var_all2=[var2;var3];
brobs2=robustfit(var_all2(:,1:3),var_all2(:,4),'logistic');

res2=brobs(1)+brobs(2)*var_all(:,1)+brobs(3)*var_all(:,2)+brobs(4)*var_all(:,3);
res3=brobs2(1)+brobs2(2)*var_all(:,1)+brobs2(3)*var_all(:,2)+brobs2(4)*var_all(:,3);

res=zeros(size(res2));
res(res2<1.5,:)=1;
res(res2>=1.5 & res3<1.5,:)=2;
res(res2>=1.5 & res3>=1.5,:)=3;
%{
res2(res2>1.5,:)=2;
res2(res2<=1.5,:)=1;

var2(:,4)=1;
var3(:,4)=2;
var_all=[var2;var3];
brobs=robustfit(var_all(:,1:3),var_all(:,4),'logistic');
res2=brobs(1)+brobs(2)*var_all(:,1)+brobs(3)*var_all(:,2)+brobs(4)*var_all(:,3);
res2(res2>1.5,:)=2;
res2(res2<=1.5,:)=1;
res2=[res2 var_all(:,4)];
%res2=mnrval(t,var_all(:,1:3));
%[~,res3]=max(res2');
%res3=[res3' var_all(:,4)];
%}