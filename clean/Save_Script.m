start_chunk=1;
end_chunk=6000;

addpath jsonlab

folder='F:\HERE\Vertical_res\Drive_2';
in_folder='F:\HERE\Drive_2';

for i=start_chunk:end_chunk
    fprintf(2,'processing chunk %d\n',i);
    if ~exist([folder,'\chunk_',num2str(i),'_res.txt'])
        continue;
    end
    fid=fopen([folder,'\chunk_',num2str(i),'_res.txt']);
    fgets(fid);
    res_A=fscanf(fid,'%f %f %f %f %f %f\n',[6 inf]);
    res_A=res_A';
    fclose(fid);
    fuse_name=[in_folder,'\chunk_',num2str(i),'.fuse'];
    fuse_A=textread(fuse_name);
    fuse_A=fuse_A(:,1:3);
    json_out=[folder,'\chunk_',num2str(i),'.json'];
    coeff_name=[in_folder,'\coeff_',num2str(i),'.txt'];
    coeff=textread(coeff_name);
    coeff=coeff(1,1:4);
    ground=coeff(4);
    Save(res_A,fuse_A,ground,json_out);
end
