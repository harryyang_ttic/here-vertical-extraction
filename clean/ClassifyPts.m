function [ V_classify ] = ClassifyPts( A,res,dist,height )
%CLASSIFYPTS Summary of this function goes here
%   Detailed explanation goes here

%% param
radius=0.6;

%log_coeff=textread('train\var_coeff_1.txt');
brobs1=textread('train\brobs3.txt');
brobs2=textread('train\brobs4.txt');

%% Vertical Pts
tmp=num2cell(floor(min(A)));
[x_mini,y_mini,~]=deal(tmp{:});

grid_x=floor(A(:,1))-x_mini+1;
grid_y=floor(A(:,2))-y_mini+1;

ids=sub2ind(size(res),grid_x,grid_y);
res_id=res(ids);
height_id=height(ids);
V_id=res_id>0 & dist<height_id;
V_classify=zeros(size(V_id));

V_pts=A(V_id,:);

%% to reduce time
if size(V_pts,1)>5000
    for i=1:length(V_id)
        if V_id(i)==0
            continue;
        end
        V_classify(i)=2;
    end
    return;
end

%% 
nearest=rangesearch(A,V_pts,radius);
j=1;
for i=1:length(V_id)
    if V_id(i)==0
        continue;
    end
    A_t=A(nearest{j,1},:);
    [coeff,~,variance]=pca(A_t);
    if length(variance)<3
        V_classify(i)=1;
        j=j+1;
        continue;
    end
    variance=variance/norm(variance);
    res2=brobs1(1)+brobs1(2)*variance(1)+brobs1(3)*variance(2)+brobs1(4)*variance(3);
    res3=brobs2(1)+brobs2(2)*variance(1)+brobs2(3)*variance(2)+brobs2(4)*variance(3);
    [~,coeff_id]=max(coeff(:,1));
    if res2<1.5 && coeff_id==3
        log_id=1;
    else
        if res3<1.5
            log_id=2;
        else
            log_id=3;
        end
    end
    V_classify(i)=log_id;
    j=j+1;
end

end

