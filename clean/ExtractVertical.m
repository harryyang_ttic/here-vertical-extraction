function [] = ExtractVertical(filename, coeff_file, traj_name,bdry1_name,bdry2_name,file_out)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% default: 1,5,3,.5

A=ReadNED_function(filename);
coeff=textread(coeff_file);
coeff=coeff(1,1:4);
traj=textread(traj_name);
bdry1=textread(bdry1_name);
bdry2=textread(bdry2_name);

fprintf(2,'compute height\n');
[height,dist, height2,traj_map]=ComputeHeight(A,coeff,traj);
road_width=ComputeRoadWidth(bdry1,bdry2);
fprintf(2,'filter height\n');
[res,res2]=FilterHeight(height,height2);
res=TrajectoryFilter(res,traj_map,road_width);
fprintf(2,'classify point\n');
V_classify=ClassifyPts( A,res,dist, height );
fprintf(2,'refine\n');
res=int32(res);
res_A=Refine(A,res,height, V_classify, dist,res2);

A2=A;
A2(:,1)=A(:,1)-0.5;
A2(:,2)=A(:,2)-0.5;

fprintf(2,'compute height\n');
[height,dist, height2,traj_map]=ComputeHeight(A2,coeff,traj);
road_width=ComputeRoadWidth(bdry1,bdry2);
fprintf(2,'filter height\n');
[res,res2]=FilterHeight(height,height2);
res=TrajectoryFilter(res,traj_map,road_width);
fprintf(2,'classify point\n');
V_classify=ClassifyPts( A2,res,dist, height );
fprintf(2,'refine\n');
res=int32(res);
res_A2=Refine(A2,res,height, V_classify, dist,res2);

res_A(~ismember(res_A2(:,4:6),[255 255 255],'rows'),4:6)=res_A2(~ismember(res_A2(:,4:6),[255 255 255],'rows'),4:6);

fprintf(2,'save\n');
fid=fopen(file_out,'w');
fprintf(fid,'%d\n',size(res_A,1));
fprintf(fid,'%g %g %g %g %g %g\n',res_A');
fclose(fid);

end

