function [ res,res2 ] = FilterHeight(height,height2)
%FILTERHEIGHT Summary of this function goes here
%   Detailed explanation goes here
% default: 4,2

%% param
thresh1=5;
thresh2=4;
thresh3=3;

res=height>2;
label1=ConnectedComponentFilter(height,0.8);
for i=1:max(max(label1))
    [ss1, ss2]=find(label1==i);
    if isempty(ss1)
        continue;
    end
    heightt=max(ss1)-min(ss1)+1;
    weightt=max(ss2)-min(ss2)+1;
    if heightt>thresh1 || weightt>thresh1
        res(label1==i)=0;
    end
end

[H, W]=size(res);
for h=1:H
    for w=1:W
        h1=0;
        if res(h,w)>0
            if h>1
                h1=max(h1,height(h-1,w));
            end
            if w>1
                h1=max(h1,height(h,w-1));
            end
            if h<H
                h1=max(h1,height(h+1,w));
            end
            if w<W
                h1=max(h1,height(h,w+1));
            end
        end
       
        if h1-0.5>height(h,w)
            res(h,w)=0;
        end
    end
end

res2=res;
label2=ConnectedComponentFilter(height2,1.1);
for i=1:max(max(label2))
    [ss1, ss2]=find(label2==i);
    if isempty(ss1)
        continue;
    end
    heightt=max(ss1)-min(ss1)+1;
    weightt=max(ss2)-min(ss2)+1;
    if heightt>thresh2 || weightt>thresh2 || heightt/weightt>=thresh3 || weightt/heightt>=thresh3
        res2(label2==i)=0;
    end
end

end

