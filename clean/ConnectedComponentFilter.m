function [ label ] = ConnectedComponentFilter( height, thresh )
%CONNECTEDCOMPONENTFILTER Summary of this function goes here
%   Detailed explanation goes here

[H,W]=size(height);
label=zeros(H,W);
cur_label=1;
label_relation=cell(H*W+1,1);

%label_relation=1:1:H*W;
if isempty(height)
    return;
end

for h=1:H
    for w=1:W
        if height(h,w)>0
            L=[];
            for hh=h-1:h+1
                for ww=w-1:w+1
                    if hh>=1 && hh<=H && ww>=1 && ww<=W
                        if label(hh,ww)>0  && abs(height(hh,ww)-height(h,w))<=thresh
                            L=[L label(hh,ww)];
                        end
                    end
                end
            end
            if isempty(L)
                label(h,w)=cur_label;
                label_relation{cur_label}=[label_relation{cur_label} cur_label];
                cur_label=cur_label+1;
            else
                label(h,w)=min(L);
                for i=1:length(L)
                    for j=1:length(L)
                        label_relation{L(i)}=union(label_relation{L(i)},L(j));
                    end
                end
            end
        end
    end
end

mat=zeros(cur_label,cur_label);
for i=1:cur_label
    for j=1:length(label_relation{i})
        mat(i,label_relation{i}(j))=1;
    end
end
[~,CC]=graphconncomp(mat);

for h=1:H
    for w=1:W
        if label(h,w)>0
            label(h,w)=CC(label(h,w));
        end
    end
end



end

