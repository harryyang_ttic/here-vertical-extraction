start_chunk=1000;
end_chunk=1000;


for i=start_chunk:end_chunk
    fprintf(2,'processing chunk %d\n',i);
    
    chunk_name=['F:HERE\Drive_1\chunk_',num2str(i),'.ned'];
    traj_name=['F:HERE\Drive_1\chunk_',num2str(i),'.traj'];
    bdry1_name=['F:HERE\Drive_1\chunk_',num2str(i),'.bdry1'];
    bdry2_name=['F:HERE\Drive_1\chunk_',num2str(i),'.bdry2'];
    coeff_name=['F:HERE\Drive_1\coeff_',num2str(i),'.txt'];
    res=['F:HERE\Vertical_res\Drive_1\chunk_',num2str(i),'_res.txt'];
    if ~(exist(chunk_name) && exist(traj_name) && exist(bdry1_name) && exist(bdry2_name) && exist(coeff_name))
        continue;
    end
   
    ExtractVertical(chunk_name,coeff_name,traj_name,bdry1_name,bdry2_name,res);
end
