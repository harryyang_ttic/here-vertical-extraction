function [ res2 ] = TrajectoryFilter( res,traj_map,road_width )
%TRAJECTORYFILTER Summary of this function goes here
%   Detailed explanation goes here

[H,W]=size(res);
[x, y]=find(traj_map>0);
p=polyfit(x,y,1);
t=1:1:H*W;
[x2, y2]=ind2sub(size(res),t);
if p(1)==inf
    dist=abs(x2'-x(1));
else
    dist=abs(p(1)*x2'-y2'+p(2))/sqrt(p(1)*p(1)+1);
end
res2=res;
res2(dist>min(14,road_width/2+max(4,road_width/2)))=0;


end

