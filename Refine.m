function [ res_A, res ] = Refine( A,res,height, V_classify, dist, res2)
%CLASSIFYPTS2 Summary of this function goes here
%   Detailed explanation goes here

%% params
ground_thresh=1;
is_tree_thresh1=5;
is_tree_thresh2=0.88;
<<<<<<< HEAD
point_num_thresh=12;
=======
point_num_thresh=15;
point_num_thresh2=0.8;
>>>>>>> 1cd862bc931bda25f239643ff304c65474f7d93f

%% refine
tmp=num2cell(floor(min(A)));
[x_mini,y_mini,z_mini]=deal(tmp{:});

tmp=num2cell(floor(max(A)));
[x_maxi,y_maxi,z_maxi]=deal(tmp{:});

[~,~,~,loc]=histcn(A(:,1:2),x_mini:1:x_maxi+1,y_mini:1:y_maxi+1);
[loc2,~,ic]=unique(loc,'rows');

binranges=ground_thresh:0.5:z_maxi-z_mini+1;

for i=1:size(loc2,1)
    if res(loc2(i,1),loc2(i,2))==0
        V_classify(ic==i)=0;
        continue;
    end
    classi_g=V_classify(ic==i & dist<height(loc2(i,1),loc2(i,2)));
    dist_g=dist(ic==i & dist<height(loc2(i,1),loc2(i,2)));
    pts_g=A(ic==i & dist<height(loc2(i,1),loc2(i,2)),:);
    [~,~,~,loc3]=histcn(dist_g,binranges);
    grid_structure=zeros(max(loc3),1);
    for j=1:max(loc3)
        this_class=classi_g(loc3==j);
        line_cnt=sum(this_class==1);
        other_cnt=sum(this_class~=1);
        if line_cnt>other_cnt
            grid_structure(j)=1;
        end
    end
    chunks=[0 length(grid_structure)]';
    grid_diff=diff(grid_structure);
    chunks=[chunks;find(grid_diff)];
    chunks=sort(chunks);
    for ii=1:length(chunks)-1
        start=chunks(ii)+1;
        endd=chunks(ii+1);
        if grid_structure(start)==1
            continue;
        end
        pts_g_step=pts_g(loc3>=start & loc3<=endd,:);
        [~,~,latent]=pca(pts_g_step(:,1:2));
        if ~isempty(latent) && length(latent)>1 && latent(2)>latent(1)/3
            % trees
            V_classify(ic==i & dist>=(start-1)*0.5+1 & dist<endd*0.5+1)=3;
        else
            V_classify(ic==i & dist>=(start-1)*0.5+1 & dist<endd*0.5+1)=2;
        end
    end

    if sum(grid_structure==1)<=sum(grid_structure~=1)/2 && res2(loc2(i,1),loc2(i,2))==0
        V_classify(ic==i)=0;
        res(loc2(i,1),loc2(i,2))=0;
    end
    
    line_pt=sum(V_classify(ic==i)==1);
    other_pt=sum(V_classify(ic==i)==3);
    total_pt=sum(V_classify(ic==i)>0);
  
   
    if line_pt<is_tree_thresh1 && other_pt/total_pt>is_tree_thresh2
        V_classify(ic==i)=0;
        res(loc2(i,1),loc2(i,2))=0;
    end
<<<<<<< HEAD
    if total_pt<point_num_thresh
=======
    if total_pt<point_num_thresh 
>>>>>>> 1cd862bc931bda25f239643ff304c65474f7d93f
        V_classify(ic==i)=0;
        res(loc2(i,1),loc2(i,2))=0;
    end
end

res_A=A;
res_A(:,4:6)=255;

res_A(V_classify==1,5:6)=0;  % line
res_A(V_classify==2,[4,6])=0; % plane
res_A(V_classify==3,[4,5])=0; % tree

end

