
start_chunk=1059;
end_chunk=1059;

start_chunk=1502;
end_chunk=1502;



for i=start_chunk:end_chunk
    fprintf(2,'processing chunk %d\n',i);
    
    chunk_name=['D:\start_1000\chunk_',num2str(i),'.ned'];
    traj_name=['D:\start_1000\chunk_',num2str(i),'.traj'];
    bdry1_name=['D:\start_1000\chunk_',num2str(i),'.bdry1'];
    bdry2_name=['D:\start_1000\chunk_',num2str(i),'.bdry2'];
    coeff_name=['D:\start_1000\coeff_',num2str(i),'.txt'];
    res=['train_baseline\chunk_',num2str(i),'_res.txt'];
    if ~(exist(chunk_name) && exist(traj_name) && exist(bdry1_name) && exist(bdry2_name) && exist(coeff_name))
        continue;
    end
    %{
    if ~exist(res)
      continue;
    end
    %}
    ExtractVertical(chunk_name,coeff_name,traj_name,bdry1_name,bdry2_name,res);
end
