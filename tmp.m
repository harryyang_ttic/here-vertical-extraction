%{
fid=fopen('res\chunk_1001_res.txt');
fgets(fid);
res_A=fscanf(fid,'%f %f %f %f %f %f\n',[6 inf]);
res_A=res_A';
fclose(fid);
fuse_name='D:\start_1000\chunk_1001.fuse';
fuse_A=textread(fuse_name);
fuse_A=fuse_A(:,1:3);
json_out='chunk_1001.json';
Save(res_A,fuse_A,json_out);


p=zeros(1001,1);
for i=1000:2000
    filename=['old_res2\chunk_',num2str(i),'.json'];
    A=loadjson(filename);
    num=length(A.vertical_structure);
    p(i)=num;
end

for i=1000:1500

    src=['res2\chunk_',num2str(i),'_res.txt'];
    if ~exist(src)
        continue;
    end
    tmp=['res\chunk_',num2str(i),'_r.txt'];
    if exist(tmp)
        continue;
    end
    dst=['res\chunk_',num2str(i),'_res.txt'];
    copyfile(src,dst);

end
%}

pp=zeros(1001,1);
min_num=10000;
ii=0;
jj=0;
Q=zeros(0,4);
id=1;
for i=1000:2000
    filename=['res\chunk_',num2str(i),'.json'];
    if ~exist(filename)
        continue;
    end
    A=loadjson(filename);
    num=length(A.vertical_structure);
    vs=A.vertical_structure;
    if num==1
        min_num=min(min_num,vs.point_num);
        Q(id,:)=[i 1 vs.point_num vs.height];
        id=id+1;
        if min_num==vs.point_num
            ii=i;
            jj=1;
        end
    elseif num>1
        for j=1:num
            min_num=min(min_num,vs{1,j}.point_num);
            Q(id,:)=[i 1 vs{1,j}.point_num vs{1,j}.height];
            id=id+1;
            if min_num==vs{1,j}.point_num
                ii=i;
                jj=j;
            end
        end
    end
end

for i=1501:2000
    src=['res_baseline\chunk_',num2str(i),'_res.txt'];
    if ~exist(src)
        continue;
    end
    dst=['res\chunk_',num2str(i),'_res.txt'];
    if exist(dst)
        continue;
    end
    copyfile(src,dst);
end


