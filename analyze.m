res=zeros(1000,1);
id=1;
for i=1000:1500
    filename=['res\chunk_',num2str(i),'.json'];
    if ~exist(filename)
        continue;
    end
    A=loadjson(filename);
    num=length(A.vertical_structure);
    res(i-999,1)=num;
   
    fn2=['train_baseline\chunk_',num2str(i),'.json'];
    if ~exist(fn2)
        continue;
    end
    
    A=loadjson(fn2);
    num=length(A.vertical_structure);
    vs=A.vertical_structure;
    if num==1
        if vs.height<2
            num=num-1;
        elseif vs.point_num<=5
            num=num-1;
        end
    elseif num>1
        for j=1:num
            if vs{1,j}.height<2
                num=num-1;
            elseif vs{1,j}.point_num<=5
                num=num-1;
            end
        end
    end
    res(i-999,2)=num;
   
end