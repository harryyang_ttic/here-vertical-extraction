function [] = Save( res_A, fuse_A, ground,json_out )
%SAVE Summary of this function goes here
%   Detailed explanation goes here

thresh1=5;

%% get lat lon
% if there are red points take the average of red points as lat lon,
% otherwise take the average of all the points

tmp=num2cell(floor(min(res_A)));
[x_mini,y_mini,~,~,~,~]=deal(tmp{:});

tmp=num2cell(floor(max(res_A)));
[x_maxi,y_maxi,~,~,~,~]=deal(tmp{:});

res_A_v=res_A(~ismember(res_A(:,4:6),[255 255 255],'rows'),:);
fuse_A_v=fuse_A(~ismember(res_A(:,4:6),[255 255 255],'rows'),:);

all_js=zeros(1,0);
if isempty(res_A_v)
    savejson('vertical_structure',all_js,json_out);
    return;
end
    
[~,~,~,loc]=histcn(res_A_v(:,1:2),x_mini:1:x_maxi+1,y_mini:1:y_maxi+1);
[loc2,~,ic]=unique(loc,'rows');

xg=x_maxi-x_mini+1;
yg=y_maxi-y_mini+1;

H=zeros(xg,yg);

for i=1:size(loc2,1)
    ned_pts=res_A_v(ic==i,:);
    H(loc2(i,1),loc2(i,2))=max(abs(ned_pts(:,3)+ground));
end

labels=ConnectedComponentFilter(H,100);

for i=1:max(max(labels))
    [ss1, ss2]=find(labels==i);
    if isempty(ss1)
        continue;
    end
    ned_pts_all=[];
    fuse_pts_all=[];
    red_pts_all=[];
    for j=1:length(ss1)
        id=find(ismember(loc2,[ss1(j),ss2(j)],'rows'),1);
        ned_pts=res_A_v(ic==id,:);
        fuse_pts=fuse_A_v(ic==id,:);
        red_pts=fuse_pts(ismember(ned_pts(:,4:6),[255 0 0],'rows'),:);
        ned_pts_all=[ned_pts_all;ned_pts];
        fuse_pts_all=[fuse_pts_all;fuse_pts];
        red_pts_all=[red_pts_all;red_pts];
    end
    
    if size(red_pts_all,1)>=thresh1
        lat=mean(red_pts_all(:,1));
        lon=mean(red_pts_all(:,2));
    else
        lat=mean(fuse_pts_all(:,1));
        lon=mean(fuse_pts_all(:,2));
    end
    height=max(abs(ned_pts_all(:,3)+ground));
    jsonmesh=struct('lat',lat,'lon',lon,'height',height,'point_num',size(fuse_pts_all,1),'point_list',fuse_pts_all);
    all_js=[all_js jsonmesh];
end

savejson('vertical_structure',all_js,json_out);
end

