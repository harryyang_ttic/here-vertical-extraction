start_chunk=1108;
end_chunk=1108;

for i=start_chunk:end_chunk
    fprintf(2,'processing chunk %d\n',i);
    if ~exist(['res\chunk_',num2str(i),'_res.txt'])
        continue;
    end
    fid=fopen(['res\chunk_',num2str(i),'_res.txt']);
    fgets(fid);
    res_A=fscanf(fid,'%f %f %f %f %f %f\n',[6 inf]);
    res_A=res_A';
    fclose(fid);
    fuse_name=['D:\start_1000\chunk_',num2str(i),'.fuse'];
    fuse_A=textread(fuse_name);
    fuse_A=fuse_A(:,1:3);
    json_out=['res\chunk_',num2str(i),'.json'];
    coeff_name=['D:\start_1000\coeff_',num2str(i),'.txt'];
    coeff=textread(coeff_name);
    coeff=coeff(1,1:4);
    ground=coeff(4);
    Save(res_A,fuse_A,ground,json_out);
end


for i=start_chunk:end_chunk
    fprintf(2,'processing chunk %d\n',i);
    if ~exist(['train_baseline\chunk_',num2str(i),'_res.txt'])
        continue;
    end
    fid=fopen(['train_baseline\chunk_',num2str(i),'_res.txt']);
    fgets(fid);
    res_A=fscanf(fid,'%f %f %f %f %f %f\n',[6 inf]);
    res_A=res_A';
    fclose(fid);
    fuse_name=['D:\start_1000\chunk_',num2str(i),'.fuse'];
    fuse_A=textread(fuse_name);
    fuse_A=fuse_A(:,1:3);
    json_out=['train_baseline\chunk_',num2str(i),'.json'];
    coeff_name=['D:\start_1000\coeff_',num2str(i),'.txt'];
    coeff=textread(coeff_name);
    coeff=coeff(1,1:4);
    ground=coeff(4);
    Save(res_A,fuse_A,ground,json_out);
end
