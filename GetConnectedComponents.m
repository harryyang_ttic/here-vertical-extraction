function [Ac] = GetConnectedComponents( A,coeff)
%GETCONNECTEDCOMPONENTS Summary of this function goes here
%   Detailed explanation goes here

%% params
have_point_test1=1.5;
have_point_test2=2;

%% discretize
tmp=num2cell(floor(min(A)));
[x_mini,y_mini,~]=deal(tmp{:});
tmp=num2cell(floor(max(A)));
[x_maxi,y_maxi,~]=deal(tmp{:});

xg=x_maxi-x_mini+1;
yg=y_maxi-y_mini+1;

res=zeros(xg,yg);

tmp=num2cell(coeff);
[a,b,c,d]=deal(tmp{:});
dist=abs(a*A(:,1)+b*A(:,2)+c*A(:,3)+d)/sqrt(a*a+b*b+c*c);

[~,~,~,loc]=histcn(A(:,1:2),x_mini:1:x_maxi+1,y_mini:1:y_maxi+1);
[loc2,~,ic]=unique(loc,'rows');

for i=1:length(loc2)
    list=dist(ic==i);
    if sum(list>have_point_test1)>=have_point_test2
        res(loc2(i,1),loc2(i,2))=1;
    end
end

%% connected components
label = ConnectedComponentFilter(res,0);

Ac=A;
Ac(:,4)=0;
for i=1:length(loc2)
    Ac(ic==i,4)=label(loc2(i,1),loc2(i,2));
end

