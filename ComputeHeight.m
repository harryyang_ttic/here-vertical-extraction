function [ height, dist,height2, traj_map ] = ComputeHeight( A, coeff, traj )
%COMPUTEHEIGHT Summary of this function goes here
%   Detailed explanation goes here

%% parameters
ground_thresh=1;
have_point_test1=1.5;
have_point_test2=2;

%% discretize and compute distance
tmp=num2cell(floor(min(A)));
[x_mini,y_mini,z_mini]=deal(tmp{:});
tmp=num2cell(floor(max(A)));
[x_maxi,y_maxi,z_maxi]=deal(tmp{:});

xg=x_maxi-x_mini+1;
yg=y_maxi-y_mini+1;

tmp=num2cell(coeff);
[~,~,~,d]=deal(tmp{:});

<<<<<<< HEAD
=======
%dist=abs(a*A(:,1)+b*A(:,2)+c*A(:,3)+d)/sqrt(a*a+b*b+c*c);
>>>>>>> 1cd862bc931bda25f239643ff304c65474f7d93f
if sum(A(:,3)+d>0)>sum(-A(:,3)-d>0)
    dist=A(:,3)+d;
else
    dist=-A(:,3)-d;
end

[~,~,~,loc]=histcn(A(:,1:2),x_mini:1:x_maxi+1,y_mini:1:y_maxi+1);

height=zeros(xg,yg);
height2=zeros(xg,yg);
binranges=ground_thresh:0.5:z_maxi-z_mini+1;

[loc2,~,ic]=unique(loc,'rows');
for i=1:size(loc2,1)
    list=dist(ic==i);
    list=sort(list);
    histg=histc(list,binranges); 
    
    histgg=find(histg);
    if isempty(histgg)
        max_height=0;
    else
        max_height=histgg(end);
    end
    ii=0;
    for ii=max_height:-1:0
        if ii==0
            break;
        end
        line_c=sum(histg(1:ii)>0);
        non_line_c=sum(histg(1:ii)==0);
        if line_c>=non_line_c*2
            break;
        end
    end
    max_dist=max(dist(ic==i & dist<ground_thresh+0.5*ii));
    if isempty(max_dist)
        max_dist=0;
    end
    height(loc2(i,1),loc2(i,2))=max_dist;
    if sum(list>have_point_test1)>have_point_test2
        height2(loc2(i,1),loc2(i,2))=max(list);
    end
end

[~,~,~,loc]=histcn(traj(:,1:2),x_mini:1:x_maxi+1,y_mini:1:y_maxi+1);
[loc2,~,~]=unique(loc,'rows');
traj_map=zeros(xg,yg);
ids=sub2ind([xg,yg],loc2(:,1),loc2(:,2));
traj_map(ids)=1;

end

