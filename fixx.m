%{
for i=1500:1550
    name=['res/error2/chunk_',num2str(i),'_res_res.txt'];
    if exist(name)
        name2=['res/error2/chunk_',num2str(i),'_res.txt'];
        movefile(name,name2);
    end
end
%}
%% add fix

for i=1010:1010
    name=['res/error/chunk_',num2str(i),'_res.xyz'];
    if exist(name)
        fid=fopen(name);
        A=fscanf(fid,'%f %f %f\',[3 inf]);
        A=A';
        fclose(fid);
        
        name2=['res/error/chunk_',num2str(i),'_res.txt'];
        if ~exist(name2)
            continue;
        end
        fid=fopen(name2);
        fgets(fid);
        A2=fscanf(fid,'%f %f %f %f %f %f\',[6 inf]);
        A2=A2';
        fclose(fid);
        
        idx=knnsearch(A2(:,1:3),A);
        idx2=1:1:size(A2,1);
        idx=setdiff(idx2',idx);
        if isempty(idx)
            continue;
        end
        color=A2(idx,4:6);
        if sum(sum(color~=255))>0
            A2(idx,4:6)=255;
        else
            A2(idx,4)=255;
            A2(idx,5:6)=0;
        end
        name2=['res/error/chunk_',num2str(i),'_res.txt'];
        fid=fopen(name2,'w');
        fprintf(fid,'%d\n',size(A2,1));
        fprintf(fid,'%g %g %g %g %g %g\n',A2');
        fclose(fid);
    end
end
